const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

let temporalStorage = {
    todos: [
        {
            id: 1,
            title: 'test title',
            text: 'test text',
            status: 'in progress',
            time: 0,
            created: new Date().getTime()
        },
        {
            id: 2,
            title: 'second test title',
            text: 'one more test text',
            status: 'in progress',
            time: 0,
            created: new Date().getTime()
        }
    ]
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

app.get('/', (req, res) => {
    res.send(`You requested list of todos`);
});

app.post('/addTodo', (req, res) => {
    let todo = {
        id: temporalStorage.todos.length + 1,
        title: req.body.title,
        text: req.body.text,
        status: 'in progress',
        time: 0,
        created: new Date().getTime()
    };
    temporalStorage.todos.push(todo);
    res.sendStatus(200);
});

app.get('/getTodos', (req, res) => {
    const body = temporalStorage.todos;
    res.set('Content-Type', 'application/json');
    res.send(JSON.stringify(body));
});

app.get('/removeTodo/:id', (req, res) => {
    let todos = temporalStorage.todos;
    const index = todos.findIndex(todo => Number(todo.id) === Number(req.params.id));
    if (index !== -1) {
        todos.splice(index, 1);
        res.sendStatus(200);
    } else {
        res.sendStatus(500);
    }
});

app.get('/doneTodo/:id', (req, res) => {
    let matchedTodo = changeTodoStatusById(req.params.id, 'done');
    if (matchedTodo !== null) {
        res.send(JSON.stringify(matchedTodo));
    } else {
        res.sendStatus(404);
    }
});

app.get('/failTodo/:id', (req, res) => {
    let matchedTodo = changeTodoStatusById(req.params.id, 'fail');
    if (matchedTodo !== null) {
        res.send(JSON.stringify(matchedTodo));
    } else {
        res.sendStatus(404);
    }
});

function changeTodoStatusById(id, status) {
    let todos = temporalStorage.todos;
    let matchedTodo = null;
    for (let todo of todos) {
        if (Number(todo.id) === Number(id)) {
            todo.status = status;
            todo.time = new Date().getTime() - todo.created;
            matchedTodo = todo;
            break;
        }
    }

    return matchedTodo;
}

app.listen(3001, err => {
    if (err) {
        throw err;
    }

    console.log('Server started on port 3001');
});
